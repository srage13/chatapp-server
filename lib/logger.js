/**
 * Logger
 * @author Saish
 */


const fs = require('fs');
const winston = require('winston');
const moment = require('moment');

class Logger {
    constructor() {
        let date = moment();
        let currentDateTime = date.format('YYYY-MM-DD');
        const logger = winston.createLogger({
            transports: [
                new winston.transports.Console(),
                new winston.transports.File({
                    filename: `${__dirname}/../logs/${currentDateTime}.log`
                })
            ],
            
            format: winston.format.printf((info) => {
                console.log(`${__dirname}..logs/${currentDateTime}.log`);
                let message = `${date.format('MMMM Do YYYY, h:mm:ss a')} | ${info.level.toUpperCase()} | ${info.message} `;
                console.log(info.message);
                return message;
            })
        });
        this.logger = logger
    }

    async info(message) {
        this.logger.log('info', message);
    }

    async error(message) {
        this.logger.log('error', message);
    }
}

module.exports = Logger;