const Sequelize = require('sequelize');
const db = require('../config/dbconfig');

const user = db.define('user', {
  user_id: {
    type: Sequelize.INTEGER(11),
    allowNull: false,
    primaryKey: true,
    autoIncrement: true
  },
  username: {
    type: Sequelize.STRING(100),
    allowNull: true
  },
  email: {
    type: Sequelize.STRING(100),
    allowNull: true
  },
  password: {
    type: Sequelize.STRING(255),
    allowNull: true
  }
}, {
  tableName: 'user',
  timestamps: false
});


module.exports = user;