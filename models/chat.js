const Sequelize = require('sequelize');
const db = require('../config/dbconfig');

const user = db.define('user', {
  user_id: {
    type: Sequelize.INTEGER(11),
    allowNull: false,
    primaryKey: true,
    autoIncrement: true
  },
  name: {
    type: Sequelize.STRING(100),
    allowNull: true
  },
  email: {
    type: Sequelize.STRING(100),
    allowNull: true
  },
  contact_number: {
    type: Sequelize.STRING(15),
    allowNull: true
  },
  password: {
    type: Sequelize.STRING(255),
    allowNull: true
  },
  user_name: {
    type: Sequelize.STRING(100),
    allowNull: true
  },
  job_id: {
    type: Sequelize.INTEGER(11),
    allowNull: true,
    references: {
      model: 'job',
      key: 'job_id'
    }
  }
}, {
  tableName: 'user',
  timestamps: false
});


module.exports = user;