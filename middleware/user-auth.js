const jwt = require('jsonwebtoken');
const config = require('../config/config');
const db = require('../config/dbconfig');
const Job = require('../models/chat');
const Logger = require('../lib/logger');

const logger = new Logger();

module.exports = (req, res, next) => {
  const authHeader = req.get('Authorization');
  if (!authHeader) {
    const error = new Error('Not authenticated.');
    logger.error(error + " Accessed by "+ req.ip);
    error.statusCode = 401;
    res.sendStatus(401);
    throw error;
  }
  const token = authHeader.split(' ')[1];
  let decodedToken;
  try {
    decodedToken = jwt.verify(token, config.secrets.jwtUserSalt);
  } catch (err) {
    err.statusCode = 500;
    logger.error(err);
    res.sendStatus(500);
    throw err;
  }
  if (!decodedToken) {
    const error = new Error('Not authenticated.');
    error.statusCode = 401;
    res.sendStatus(401);
    logger.error(error + " Accessed by "+ req.ip);
    throw error;
  }
  console.log(decodedToken);
  req.userId = decodedToken.userId;

  next();
};
