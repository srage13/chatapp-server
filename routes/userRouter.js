const express = require('express');
const router = express.Router();
const User = require('../models/user');
const userAuth = require('../middleware/user-auth');
const Logger = require('../lib/logger');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const config = require('../config/config');

const logger = new Logger();

router.post('/register', (req, res) => {

    if(req.body.email == '' || req.body.userName == '' || req.body.password == '')
        res.status(200).send({success: false, errorMessage: 'You have may have missed one or multiple fields, Please check and try again.'});
    let accessToken = null;
    let userId = null;
    let encPassword = bcrypt.hashSync(req.body.password, 10);

    const checkIfUserExists = () => {
        return User.count({ where: { username: req.body.userName, email: req.body.email } })
        .then(count => {
            if (count == 1) return true;
            else return false;
        });
    }

    checkIfUserExists().then(userExists => {
        if(!userExists) {
            User.create({
                email: req.body.email,
                username: req.body.userName,
                password: encPassword,
            })
            .then((user) => {
                userId = user.user_id;
                accessToken = jwt.sign({ user: userId }, config.secrets.jwtUserSalt);
                const returnData = {
                    userId, accessToken, success: true
                }
                logger.info(`Created user with ID ${user.user_id}`)
                res.status(201).send(returnData);
            })
            .catch(err => logger.info(err));
        } else {
            res.status(200).send({success: false, errorMessage: 'User already exists'});
        }
    })
    
});


router.post('/login', (req, res) => {
    let accessToken = null;
    let userId = null;
    let returnData = null;
    if(req.body.username == "" || req.body.password == "") res.status(200).send({success: false, errorMessage: 'Please enter a username or password.'});
    User.findOne({
        where: {
            username: req.body.username
        }
    })
    .then(user => {
        userId = user.user_id;
        
        bcrypt.compare(req.body.password, user.password, function(err, result) {
            if (result) {
                accessToken = jwt.sign({ user:userId }, config.secrets.jwtUserSalt);
                returnData = {
                    userId, accessToken, success: true
                }
                logger.info(`User with ID ${userId} logged in`);
                res.status(200).send(returnData);
            } else {
                returnData = {
                    success: false, errorMessage: 'Could not login, you may have entered an incorrect username or password'
                }
                logger.info(`Unsuccessfull login attempt.`);
                res.status(200).send(returnData);
            }
            if(err) {
                returnData = {
                    success: true, errorMessage: "Could not login, Please try again"
                }
                logger.error(`Could not login | error | ${err}`);
                res.status(500).send(returnData);
            }
        });
    })
    .catch(err => logger.info(err));
});




module.exports = router;