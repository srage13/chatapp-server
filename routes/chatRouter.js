const express = require('express');
const router = express.Router();
const db = require('../config/dbconfig');
const Job = require('../models/chat');
const userAuth = require('../middleware/user-auth');
const Logger = require('../lib/logger');

const logger = new Logger();

router.get('/', (req, res) => res.send("Jobs"));

router.get('/getAll', (req, res) => {
    Job.findAll()
        .then(jobs => {
            res.send(jobs);
            res.sendStatus(200);
        })
        .catch(err => logger.info(err));
});

router.get('/get/:id', userAuth, (req, res) => {
    const id = req.params.id;
    // Job.findByPk(params.id)
    Job.findAll({
        where: {
            job_id: id
        }
    })
    .then(job => {
        res.send(job);
        res.sendStatus(200);
    })
    .catch(err => logger.info(err));
});



module.exports = router;