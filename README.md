# This is a server app for a chat application

## Requirnments

- Node - v10.16.2
- NPM - v6.9.0

## Installation and Usage guide

- Run `npm install` to install all dependencies for the application.

- Create a `.env` file using the `.env_example` file as a guide this file contains the environment variables such as the host, dbname, passwords, etc. 
**Note: Do not commit and push the .env file to the repository.**

- Once the `.env` file has been configured you can run the sever using any one of the following commands
    - `npm run dev` - This use's nodemon which is installed via npm to run the server and canbe used for development environments
    - `npm run prod` - To run this command you need to first install PM2 as a global dependency, run the command `npm install pm2 -g`, Once the package has been installed you can run the `npm run prod` command to run the application via PM2. Use this for running the application in a production environment.